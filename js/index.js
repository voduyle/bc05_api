const BASE_URL = "https://635f4b183e8f65f283b01205.mockapi.io";

var idEdited = null;
function fetchAllToDo() {
    // render all todos service
    turnOnLoading();
    axios({
        url: `${BASE_URL}/todos`,
        method: "GET",
    })
        .then(function (res) {
            turnOffLoading()
            renderTodoList(res.data)
        })
        .catch(function (err) {
            turnOffLoading()
            console.log('err: ', err);
        })
}
fetchAllToDo()
//remove todos service
function removeTodo(idTodo) {
    turnOffLoading()
    axios({
        url: `${BASE_URL}/todos/${idTodo}`,
        method: "DELETE",
    })
        .then(function (res) {
            turnOffLoading()
            fetchAllToDo()
            console.log('res: ', res);
        })
        .catch(function (err) {
            turnOffLoading()
            console.log('err: ', err);
        })
}

function addToDo() {

    var data = layThongTinTuForm()
    var newTodo = {
        name: data.name,
        desc: data.desc,
        isComplete: true,
    };
    axios({
        url: `${BASE_URL}/todos`,
        method: "POST",
        data: newTodo,
    })
        .then(function (res) {
            turnOffLoading();
            fetchAllToDo();
            console.log('res: ', res);
        })
        .catch(function (err) {
            turnOffLoading();
            console.log('err: ', err);
        })
}

function editTodo(idTodo) {
    turnOnLoading()
    axios({
        url: `${BASE_URL}/todos/${idTodo}`,
        method: "GET",
    })
        .then(function (res) {
            turnOffLoading()
            //console.log('res: ', res);
            document.getElementById("name").value = res.data.name
            document.getElementById("desc").value = res.data.desc
            idEdited = res.data.id
        })
        .catch(function (err) {
            turnOffLoading()
            console.log('err: ', err);
        })
}

function updateTodo() {
    var data = layThongTinTuForm()
    axios({
        url: `${BASE_URL}/todos/${idEdited}`,
        method: "PUT",
        data: data
    })
        .then(function (res) {
            console.log(res);
            fetchAllToDo();
        })
        .catch(function (err) {
            console.log('err: ', err);
        })
}